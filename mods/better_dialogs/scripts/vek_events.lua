VekEventInfo = {
  VekKilled = {
    Odds = 20,
    { main = "VekKilled_Self" },
    { other = "VekKilled_Obs" },
  },

  BotKilled = {
    Odds = 20,
    { main = "BotKilled_Self" },
    { other = "BotKilled_Obs" },
  },

  VekKilled_Enemy = {
    Odds = 50,
    { other = "VekKilled_Vek" },
  },

  DoubleVekKill = {
    Odds = 50,
    { main = "DoubleVekKill_Self" },
    { other = "DoubleVekKill_Obs" },
  },

  DoubleVekKill_Enemy = {
    Odds = 50,
    { other = "DoubleVekKill_Vek" },
  },

  BldgDamaged = {
    Odds = 75,
    { main = "Bldg_Destroyed_Self" },
    { other = "Bldg_Destroyed_Obs" },
  },

  Bldg_Resisted = { Odds = 100 },

  BldgDamaged_Enemy = {
    Odds = 50,
    { other = "Bldg_Destroyed_Vek" },
  },

  MntDestroyed = {
    Odds = 25,
    { main = "MntDestroyed_Self" },
    { other = "MntDestroyed_Obs" },
  },

  MntDestroyed_Enemy = {
    Odds = 25,
    { other = "MntDestroyed_Vek" },
  },

  PowerCritical = { Odds = 100 },

  -- Specifically for the "Disable Immunity" pilot.
  Mech_WebBlocked = {
    Odds = 100,
    { main = "Mech_Webbed" },
  },

  Mech_Webbed = { Odds = 50 },
  Mech_Shielded = { Odds = 20 },
  Mech_Repaired = { Odds = 75 },
  Mech_ShieldDown = { Odds = 50 },

  Emerge_Detected = { Odds = 3 },
  Emerge_FailedMech = { Odds = 20 },
  Emerge_FailedVek = { Odds = 3 },
  Emerge_Success = { Odds = 3 },

  Vek_Drown = { Odds = 50 },
  Vek_Fall = { Odds = 50 },
  Vek_Smoke = { Odds = 10 },
  Vek_Frozen = { Odds = 35 },

  Pilot_Selected = { Odds = 10 },
  Pilot_Undo = { Odds = 10 },
  Pilot_Moved = { Odds = 10 },

  MissionEnd_Retreat = { Odds = 50 },
  MissionEnd_Dead = { Odds = 75 },

  Gamestart_Alien = {
    Odds = 100,
    Dialog(
      { main = "Gamestart" },
      { other = "FTL_Start" }),
  },

  Gameover = {
    Odds = 100,
    Dialog(
      { main = "Gameover_Start" },
      { other = "Gameover_Response" }),
  },

  PodDetected = {
    Odds = 100,
    Dialog(
      { main = "PodIncoming" },
      { other = "PodResponse" }),
  },

  PodDestroyed = {
    Odds = 100,
    { other = "PodDestroyed_Obs" },
  },

  PodCollected = {
    Odds = 100,
    { main = "PodCollected_Self" },
  },

  Pilot_Level = {
    Odds = 100,
    { main = "Pilot_Level_Self" },
    { other = "Pilot_Level_Obs" }
  },

  PilotDeath = {
    Odds = 100,
    { main = "Death_Main" },
    { other = "Death_Response" },
  },

  PilotDeath_Hospital = {
    Odds = 100,
    { other = "Death_Response_Medical" },
  },

  PilotDeath_AI = {
    Odds = 100,
    { other = "Death_Response_AI" },
  },

  Mission_Freeze_Mines_Vek = { Odds = 30 },
  Mission_Mines_Vek = { Odds = 30 },
  Mission_Satellite_Imminent = { Odds = 30 },
  Mission_Satellite_Launch = { Odds = 30 },
  Mission_Cataclysm_Falling = { Odds = 30 },
  Mission_Terraform_Attacks = { Odds = 30 },
  Mission_Airstrike_Incoming = { Odds = 30 },
  Mission_Lightning_Strike_Vek = { Odds = 30 },

  Mission_Factory_Spawning = { Odds = 30 },
  Mission_Reactivation_Thawed = { Odds = 30 },
  Mission_SnowStorm_FrozenVek = { Odds = 30 },
  Mission_SnowStorm_FrozenMech = { Odds = 75 },

  Mission_Disposal_Activated = { Odds = 30 },
  Mission_Barrels_Destroyed = { Odds = 30 },
  Mission_Teleporter_Mech = { Odds = 30 },
  Mission_Belt_Mech = { Odds = 30 },
}