local function init(self)
  require(self.scriptPath .. "better_dialogs")
  require(self.scriptPath .. "vek_events")
end

local function load(self, options, version)
  -- Nothing to load.
end

return {
  id = "better_dialogs",
  name = "Better Dialogs",
  init = init,
  load = load,
}