function PrepareVoiceEvent(id, pawn1, pawn2, custom_odds)
  pawn1 = pawn1 or -1
  pawn2 = pawn2 or -1

  local event = VoiceEvent(id, pawn1, pawn2)

  TriggerVoiceEvent(event, custom_odds)
end

local function validateEventPawn1(pawn1)
  if pawn1 == -1 then
    pawn1 = PAWN_ID_MECH

    if random_bool(8) then
      pawn1 = PAWN_ID_CEO
    end
  end

  if pawn1 == PAWN_ID_MECH then
    pawn1 = Game:GetRandomVoice():GetId()
  end

  return pawn1
end

local function shouldShowVoicePopup(eventInfo, custom_odds)
  local odds = eventInfo.Odds or 100

  if custom_odds >= 0 then
    odds = custom_odds
  end

  -- If this is a VoicePopup, only show if 100% chance.
  if Game:IsVoicePopup() and odds < 100 then
    return false
  end

  return odds >= random_int(100)
end

local function getVoiceCast(event)
  local cast = {
    main = event.pawn1,
    target = event.pawn2,
  }

  cast.other = Game:GetAnotherVoice(cast.main, cast.target)

  if (cast.other == -1 or random_bool(4)) and cast.main ~= PAWN_ID_CEO then
    cast.other = PAWN_ID_CEO
  end

  return cast
end

local function getSelectedDialog(eventInfo)
  local dialog = random_element(eventInfo)

  if #dialog == 0 then
    dialog = { dialog }
  end

  return dialog
end

function TriggerVoiceEvent(event, custom_odds)
  LOG("TriggerVoiceEvent for event: " .. event.id .. " and pawn1: " .. event.pawn1)

  custom_odds = custom_odds or -1
  event.pawn1 = validateEventPawn1(event.pawn1)

  local eventInfo = VekEventInfo[event.id]

  if eventInfo == nil then
    AddVoicePopup(event.id, event.pawn1)
    return
  end

  if not shouldShowVoicePopup(eventInfo, custom_odds) then
    return
  end

  if #eventInfo == 0 then
    AddVoicePopup(event.id, event.pawn1)
    return
  end

  local cast = getVoiceCast(event)
  local dialog = getSelectedDialog(eventInfo)

  for i, segment in ipairs(dialog) do
    if segment.main ~= nil then
      AddVoicePopup(segment.main, cast.main, cast)
    end

    if segment.target ~= nil then
      AddVoicePopup(segment.target, cast.target, cast)
    end

    if segment.other ~= nil then
      AddVoicePopup(segment.other, cast.other, cast)
    end

    if segment.ceo ~= nil then
      AddVoicePopup(segment.ceo, PAWN_ID_CEO, cast)
    end
  end
end

local function isPawnVoiceable(pawnId, eventId)
  if pawnId == PAWN_ID_CEO then
    return true
  end

  if Game:GetPawn(pawnId) == nil then
    return false;
  end

  if Game:GetPawn(pawnId):IsDead() then
    local isDeathEvent =
        event == "PilotDeath" or event == "Death_Main" or event == "Death_Response"

    -- Dead men tell no tales.
    if not isDeathEvent then
      return false
    end
  end

  return true
end

local function getPawnPersonality(pawnId)
  if pawnId == PAWN_ID_CEO then
    return Personality[Game:GetCorp().ceo_personality]
  end

  return Personality[Game:GetPawn(pawnId):GetPersonality()]
end

local function validateCast(cast)
  if cast == nil then
    cast = {
      main = pawnId,
      target = -1,
      other = -1,
    }
  end

  return cast
end

local function generateFinalNames(cast)
  local positions = { "main", "target", "other", "self" }
  local finalNames = {}

  for i, position in ipairs(positions) do
    local pawn = Game:GetPawn(cast[position])

    if pawn ~= nil then
      finalNames[position .. "_mech"] = pawn:GetMechName()
      finalNames[position .. "_reverse"] = pawn:GetPilotName(NAME_REVERSE)
      finalNames[position .. "_first"] = pawn:GetPilotName(NAME_FIRST)
      finalNames[position .. "_second"] = pawn:GetPilotName(NAME_SECOND)
      finalNames[position .. "_last"] = finalNames[position .. "_second"]
      finalNames[position .. "_full"] = pawn:GetPilotName(NAME_NORMAL)
    end
  end

  local corporation = Game:GetCorp()
  local ceoNames = {}

  for i in corporation.ceo_name:gmatch("%w+") do
    table.insert(ceoNames, i)
  end

  finalNames["ceo_full"] = corporation.ceo_name
  finalNames["ceo_first"] = ceoNames[1]
  finalNames["ceo_second"] = ceoNames[#ceoNames]
  finalNames["ceo_last"] = finalNames["ceo_second"]

  finalNames["corporation"] = corporation.bark_name
  finalNames["corp"] = finalNames["corporation"]

  finalNames["squad"] = Game:GetSquad()
  finalNames["saved_corp"] = Game:GetSavedCorp()

  return finalNames
end

local function createVoicePopup(pawnId, popupText, eventId)
  local popup = VoicePopup()

  popup.pawn = pawnId
  popup.text = popupText
  popup.timetravel = (eventId == "TimeTravel_Win" or eventId == "TimeTravel_Loss")

  return popup
end

function AddVoicePopup(eventId, pawnId, cast)
  if not isPawnVoiceable(pawnId, eventId) then
    return
  end

  local personality = getPawnPersonality(pawnId)

  if personality == nil then
    return
  end

  local popupText = personality:GetPilotDialog(eventId)

  cast = validateCast(cast)
  cast["self"] = pawnId

  local finalNames = generateFinalNames(cast)

  -- Just skip #corp lines if there's no valid replacement for them.
  if finalNames["corp"] == "" and string.find(popupText, "#corp") ~= nil then
    return
  end

  for tag, name in pairs(finalNames) do
    popupText = string.gsub(popupText, "#" .. tag, name)
  end

  local popup = createVoicePopup(pawnId, popupText, eventId)

  Game:AddVoicePopup(popup)
end