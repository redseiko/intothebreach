
Mission_Reclaim = Mission_Infinite:new
{
	Name = "Reclaim",
	Objectives = Objective("Reclaim this district region", 1),
}

function Mission_Reclaim:UpdateObjectives()
	Game:AddObjective("Reclaim buildings!", true)
end