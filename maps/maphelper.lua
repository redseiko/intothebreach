
MAP_LIST = {}
MAP_DIC = {}

function AddMap(mapname)
    if _G[mapname] == nil then return end
    if MAP_DIC[mapname] then
        return
    end
    MAP_LIST[#MAP_LIST+1] = mapname
    MAP_DIC[mapname] = true
end

function SearchMaps(search)	
	local ret = {}
	for i,mapname in ipairs(MAP_LIST) do   
		if mapname == "matt" or mapname == "justin" then
		--do nothing
        elseif search == "" then 
            ret[#ret + 1] = mapname
        elseif string.find(mapname,search) ~= nil then
            ret[#ret + 1] = mapname
        else
            local mapInfo = _G[mapname]
            local tags = mapInfo["tags"]
			
            if tags ~= nil then 
				local ignore = false
				local found = false
                for i,v in ipairs(tags) do
                    if v == search then
                        found = true
                    elseif v == "ignore" then
						ignore = true
					end
                end
				
				if found and not ignore then
					ret[#ret + 1] = mapname
				end
            end
        end
	end
	
	table.sort(ret)
	return ret
end

function RandomMap(tag, sector)
	local ret = {}
	for i,mapname in ipairs(MAP_LIST) do  
	    local mapObject = _G[mapname]
        local tags = mapObject["tags"]
		
		if mapname == "matt" or mapname == "justin" then
			tags = nil
		end
		
        if tags ~= nil then 
            local tag_found = false
            local sector_found = false
            for i,v in ipairs(tags) do
                if v == tag then
                    tag_found = true
                end
                if v == sector or v == "any_sector" then
                    sector_found = true
                end
            end
			
            if tag_found and sector_found then
                ret[#ret + 1] = mapname
            end
        end	
	end
	
	if #ret == 0 then
	   -- LOG("COULD NOT FIND MAP WITH TAGS "..tag.." and "..sector)
		return ""
	end
	
	--print("Possible maps = \n")
	--print(unpack(ret))
	--print("\n")
	return ret[random_int(#ret)+1]
end